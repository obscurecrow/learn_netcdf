class FileConst:
    INPUT_FILE = "../iofiles/input_file.nc"
    OUTPUT_FILE = "../iofiles/output_file.nc"


class NetcdfConst:
    TIME = "time"
    LEVEL = "level"
    LAT = "lat"
    LON = "lon"
    TEMP = "temp"
    KELVIN = "K"
