import netCDF4
from constants import FileConst as FC
from constants import NetcdfConst as NC


def main():
    read_input()


def walk_tree(top):
    values = top.groups.values()
    yield values
    for value in values:
        for children in walk_tree(value):
            yield children


def write_output():
    with netCDF4.Dataset(FC.OUTPUT_FILE, "w", "NETCDF4") as output_rootgrp:
        # we do whatever we need to do with rootgrp

        # GROUPS:

        # netCDF version 4 added support for organizing data in hierarchical groups
        forecast_group = output_rootgrp.createGroup("forecasts")
        analyses_group = output_rootgrp.createGroup("analyses")
        print(output_rootgrp.groups)
        # Groups can exist within groups in a Dataset
        forecast_group.createGroup("model1")
        output_rootgrp.createGroup("analyses/model2")
        # These two have the same result (create a subgroup in a given group)
        print(forecast_group.groups)
        # This way we will have direct access to forecasts/model2
        forecast_group_model2 = output_rootgrp.createGroup("forecasts/model2")
        print(output_rootgrp.groups)
        # If any of the intermediate elements of the path do not exist, they are created
        output_rootgrp.createGroup("foo/foo_model")
        print(output_rootgrp.groups)
        print("WALK_TREE:")
        for children in walk_tree(output_rootgrp):
            for child in children:
                print(child)

        # VARIABLES:
        # netCDF variables behave much like python multidimensional array objects
        # We can create variables both in groups and datasets

        # Name of the variable, type (f8 stands for float 64bit), dimensions
        times = output_rootgrp.createVariable(NC.TIME, "f8", (NC.TIME,))
        # i4 = 32bit signed integer
        levels = output_rootgrp.createVariable(NC.LEVEL, "i4", (NC.LEVEL,))
        # f4 = 32bit floating point
        latitudes = output_rootgrp.createVariable(NC.LAT, "f4", (NC.LAT,))
        longitudes = output_rootgrp.createVariable(NC.LON, "f4", (NC.LON,))
        temp = output_rootgrp.createVariable(NC.TEMP, "f4", (NC.TIME, NC.LEVEL, NC.LAT, NC.LON,))
        temp.units = NC.KELVIN

    # here output_rootgrp doesn't exist anymore (the file is closed)


def read_input():
    with netCDF4.Dataset(FC.INPUT_FILE, "r", "NETCDF4") as input_rootgrp:
        print(input_rootgrp.variables)


if __name__ == "__main__":
    main()
